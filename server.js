const express = require('express');
const bodyParser = require('body-parser');
const {
    reqLogger,
    resLogger,
    createEnsureDirExistMiddleware,
    createFile,
    listFiles,
    getFileData,
    updateFile,
    removeFile,
    checkFileDataBody,
    checkUrlFilename,
    checkBodyContent
} = require('./src/middleware');

const app = express();
const router = express.Router();

router.use(
    bodyParser.urlencoded({ extended: false }),
    bodyParser.json(),
    createEnsureDirExistMiddleware('files'),
    createEnsureDirExistMiddleware('logs'),
    reqLogger
);

router.get('/files/:filename', checkUrlFilename, getFileData, resLogger);

router.put('/files/:filename', checkUrlFilename, checkBodyContent, updateFile, resLogger);

router.delete('/files/:filename', checkUrlFilename, removeFile, resLogger);

router.get('/files', listFiles, resLogger);

router.post('/files', checkFileDataBody, createFile, resLogger);

app.use('/api', router);

app.listen(8080, () => {
    console.log('Listening on port 8080');
});
