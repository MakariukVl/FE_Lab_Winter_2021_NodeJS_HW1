const fs = require('fs');
const path = require('path');
const Error = require('./error');

const cwd = process.cwd();
const SUPPORTED_FILE_EXT = ['txt', 'log', 'json', 'yaml', 'xml', 'js'];
const LOG_FILE = '/logs/server.log';

const getExt = path => path.match(/(?<=\.)(\w+)$/g)[0].toLowerCase() || '';
const handleMiddlewareError = (err, res) => {
    if (err instanceof Error) {
        res.message = err.message;
        res.status(400).json(err);
    } else {
        res.message = 'Server error';
        const serverErr = new Error(res.message);
        res.status(500).json(serverErr);
    }
};

const reqLogger = (req, res, next) => {
    const url = req.baseUrl + req.url;
    const time = new Date();
    const method = req.method;
    const filePath = path.join(cwd, LOG_FILE);
    const message = `${time}\n | ${method} request received from ${url}\n`;
    console.log(message.slice(0, message.length - 1));
    fs.appendFileSync(filePath, message);
    next();
};

const resLogger = (req, res, next) => {
    const filePath = path.join(cwd, LOG_FILE);
    const status = res.statusCode;
    const statusMsg = res.statusMessage;
    const details = res.message || 'Details placeholder';
    const message = ` | ${status} ${statusMsg} - ${details}\n`;
    console.log(message.slice(0, message.length - 1));
    fs.appendFileSync(filePath, message);
    next();
};

const createEnsureDirExistMiddleware = relName => {
    return (req, res, next) => {
        const fullDir = path.join(cwd, `/${relName}`);
        req[`${relName}Dir`] = fullDir;
        if (!fs.existsSync(fullDir)) {
            fs.mkdirSync(fullDir, 0744);
            // console.log(`Directory '${relName}' created!`);
        }
        next();
    };
};

const checkFileDataBody = (req, res, next) => {
    const template = propName => `Please specify '${propName}' parameter`;
    if (!req.body.filename) {
        const message = template('filename');
        res.error = new Error(message);
    }
    if (!req.body.content) {
        const message = template('content');
        res.error = new Error(message);
    }
    next();
};

const appendFile = (req, res, next) => {
    try {
        if (res.error) {
            throw res.error;
        }
        const filePath = path.join(cwd, `./files/${req.body.filename}`);
        const extension = getExt(req.body.filename);
        if (!fs.existsSync(filePath)) {
            throw new Error(`File '${req.body.filename}' is not exist`);
        }
        if (!SUPPORTED_FILE_EXT.includes(extension)) {
            throw new Error(`Extension '${extension}' is not supported`);
        }
        fs.appendFileSync(filePath, req.body.content);
        res.message = 'File appended successfully';
        res.json({ message: res.message });
    } catch (err) {
        handleMiddlewareError(err, res);
    } finally {
        next();
    }
};

const createFile = (req, res, next) => {
    try {
        if (res.error) {
            throw res.error;
        }
        const filePath = path.join(cwd, `./files/${req.body.filename}`);
        const extension = getExt(req.body.filename);
        if (fs.existsSync(filePath)) {
            throw new Error(`File '${req.body.filename}' is already exist`);
        }
        if (!SUPPORTED_FILE_EXT.includes(extension)) {
            throw new Error(`Extension '${extension}' is not supported`);
        }
        fs.writeFileSync(filePath, req.body.content);
        res.message = 'File created successfully';
        res.json({ message: res.message });
    } catch (err) {
        handleMiddlewareError(err, res);
    } finally {
        next();
    }
};

const listFiles = (req, res, next) => {
    try {
        if (JSON.stringify(req.body) !== '{}') {
            throw new Error('Client error');
        }
        if (JSON.stringify(req.query) !== '{}') {
            throw new Error('Client error');
        }
        const files = fs.readdirSync(req.filesDir) || [];
        res.message = 'Success';
        res.json({ message: res.message, files });
    } catch (err) {
        handleMiddlewareError(err, res);
    } finally {
        next();
    }
};

const checkUrlFilename = (req, res, next) => {
    if (!req.params.filename) {
        res.error = new Error("Please specify 'filename' parameter");
    }
    next();
};

const checkBodyContent = (req, res, next) => {
    if (!req.body.content) {
        res.error = new Error("Please specify 'content' parameter");
    }
    next();
}

const getFileData = (req, res, next) => {
    try {
        if (res.error) {
            throw res.error;
        }
        const filePath = path.join(cwd, `/files/${req.params.filename}`);
        if (!fs.existsSync(filePath)) {
            throw new Error(`No file with '${req.params.filename}' filename found`);
        }
        res.message = 'Success';
        const content = fs.readFileSync(filePath, { encoding: 'utf8' });
        const extension = getExt(filePath);
        const uploadedDate = fs.statSync(filePath).birthtime;
        res.json({
            message: res.message,
            filename: req.params.filename,
            content,
            extension,
            uploadedDate
        });
    } catch (err) {
        handleMiddlewareError(err, res);
    } finally {
        next();
    }
};

const updateFile = (req, res, next) => {
    try {
        if (res.error) {
            throw res.error;
        }
        const filePath = path.join(cwd, `/files/${req.params.filename}`);
        const extension = getExt(req.params.filename);
        if (!fs.existsSync(filePath)) {
            throw new Error(`No file with '${req.params.filename}' filename found`);
        }
        if (!SUPPORTED_FILE_EXT.includes(extension)) {
            throw new Error(`Extension '${extension}' is not supported`);
        }
        fs.writeFileSync(filePath, req.body.content);
        res.message = 'File successfully updated';
        res.json({ message: res.message });
    } catch (err) {
        handleMiddlewareError(err, res);
    } finally {
        next();
    }
};

const removeFile = (req, res, next) => {
    try {
        if (res.error) {
            throw res.error;
        }
        const filePath = path.join(cwd, `/files/${req.params.filename}`);
        if (!fs.existsSync(filePath)) {
            throw new Error(`No file with '${req.params.filename}' filename found`);
        }
        fs.unlinkSync(filePath);
        res.message = 'File successfully deleted';
        res.json({ message: res.message });
    } catch (err) {
        handleMiddlewareError(err, res);
    } finally {
        next();
    }
};

module.exports = {
    reqLogger,
    resLogger,
    createEnsureDirExistMiddleware,
    checkFileDataBody,
    appendFile,
    createFile,
    listFiles,
    checkUrlFilename,
    checkBodyContent,
    getFileData,
    updateFile,
    removeFile
};
