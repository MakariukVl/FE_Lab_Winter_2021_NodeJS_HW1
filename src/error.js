module.exports = class Error {
  /**
   * Creates an instance of server Error
   * @param {string} message - Error message
   */
  constructor(message) {
    this.message = message;
  }
};